package com.example.luis.test_mysql;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.annotation.FloatRange;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

public class MainActivity extends AppCompatActivity {

    /*StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build(); */

    /*declarar variables del tipo de elemento añadido en el view, las cuales luego se relacionaran con el id dado al elemento en la vista*/
    TextView mensaje1, mensaje2, mensaje5, mensaje6, mensaje7,mensaje8, txt_err_mysql;
    EditText err_mysql;
    String latitud, error_mysql;
    ejecutar_consulta ejecutar_cons = new ejecutar_consulta();

    String consulta_de_mysql = "";

    Float metros_segundo;
    Integer kilometros_hora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*enableStrictMode(); */

        /* asigno las variables del tipo textview (en este caso) a los correspondientes id en el view */
        mensaje1 = (TextView) findViewById(R.id.txt_mysql1);
        mensaje2 = (TextView) findViewById(R.id.txt_mysql2);
        mensaje5 = (TextView) findViewById(R.id.txt_id1);
        mensaje6 = (TextView) findViewById(R.id.txt_id2);
        mensaje7 = (TextView) findViewById(R.id.txt_error);
        mensaje8 = (TextView) findViewById(R.id.txt_km_hora);

        err_mysql = (EditText) findViewById(R.id.error_class_mysq);

        txt_err_mysql = (TextView) findViewById(R.id.txt_error_clase_mysql);

        new thread_mysql().execute();



        /* Uso de la clase LocationManager para obtener la localizacion del GPS */
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new  Localizacion();
        Local.setMainActivity(this);
        try{
            mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
                    (LocationListener) Local);
        }
        catch (SecurityException e){
            mensaje7.setText(e.getMessage());

        }


        //startService(new Intent(this, Servicioenviodatoslatlong.class));

        Intent intent = new Intent(this, Servicioenviodatoslatlong.class);
        startService(intent);

        //instancear metodos que estan dentro de mi servicio
        /*Servicioenviodatoslatlong obj = new Servicioenviodatoslatlong();
        obj.ejecutar_mysql("asd"); */


        /*mensaje5.setText("Localizacion agregada");
        mensaje6.setText(""); */

        /*conectar_mysql();*/
    }

    public void setLocation(Location loc) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);
                    mensaje6.setText("Mi direccion es: \n"
                            + DirCalle.getAddressLine(0));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class thread_mysql extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            conectar_mysql();
            return null;
        }
        protected void onProgressUpdate(Void... values) {

        }
        protected void onPostExecute(String result) {
            /*para ejecutar cambios dentro de mi ui debo usar el metodo post execute (o el que se requiera, distinto de doinbackground */
            mensaje2.setText(error_mysql);
            mensaje1.setText(latitud);

        }
    }

    class eject_cons extends  AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... params){
            ejecutar_mysql(consulta_de_mysql);
            return null;
        }
        @Override
        protected void onPostExecute(String result){
            err_mysql.setText(error_mysql);
            /*limpiar variable que contiene/contenia la consulta para evitar posibles errores*/
            consulta_de_mysql="";
        }

    }

    void ejecutar_mysql(String recibo_cons) {
        try {
            String url = "jdbc:mysql://170.239.84.36/coordenadas_gps";
            String user = "gps_usuario";
            String password = "aiep";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = DriverManager.getConnection(url, user, password);
            Statement st = con.createStatement();

            st.executeUpdate(recibo_cons);
        }
        catch (Exception e){
            /*err_mysql.setText(e.getMessage()); */
            error_mysql = e.getMessage();
        }
    }

    /*funcion para elminar restriccion de hilos dentro de la aplicacion */
    public void enableStrictMode()
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void conectar_mysql(){
        try {
            String url = "jdbc:mysql://170.239.84.36/coordenadas_gps";
            String user = "gps_usuario";
            String password = "aiep";

            // The newInstance() call is a work around for some broken Java implementations
            //con esto nos aseguramos de que se crean los recursos estaticos necesarios
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = DriverManager.getConnection(url, user, password);
            Statement st = con.createStatement();
            ResultSet rs = st
                    .executeQuery("select latitud from datos where id='1'");
            /*mensaje2.setText("paso por la conexion sin dar errores"); */
            error_mysql = "paso por la conexion";
            while (rs.next()) {

                /*mensaje1.setText(rs.getString("latitud"));*/
                latitud = rs.getString("latitud");


                //en cada iteracion tenemos un resultado concreto
                //si la query es un "select una_columna, otra_columna from ..." recuerda que rs tiene funciones tales como
                //rs.getString(0) //devuelve una_columna, haciendo cast a string
                //rs.getDouble(1) //devuelve otra_columna, haciendo cast a Double
                //etc, que reciben el indice de los valor, y nos devueven el valor haciendo el cast correspondiente.
            }
        }
        catch (Exception e) {
            error_mysql =e.getMessage();
            e.printStackTrace();
        }
    }


    public void conversion_float_int(Float mt_hora) {

        Double multiplo = 3.6;
        Double metros_horas = Double.parseDouble(Float.toString(mt_hora));
        Double resultado_en_kmhora;

        resultado_en_kmhora = multiplo*metros_horas;

       /* mensaje8.setText(Double.toString(resultado_en_kmhora)); */

    }


    public void actualizar_km_hora(){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable(){
                    @Override
                    public void run(){


                    }
                });

            }
        }, 0, 1000);

    }

    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        MainActivity mainActivity;
        private Location mLastLocation;

        public MainActivity getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(MainActivity mainActivity) {
            this.mainActivity = mainActivity;
        }

        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion

            loc.getLatitude();
            loc.getLongitude();
            loc.getSpeed();
            loc.hasSpeed();

            //calcul manually speed
            double speed = 0;
            if (this.mLastLocation != null)
                speed = (Math.sqrt(Math.pow(loc.getLongitude() - mLastLocation.getLongitude(), 2) + Math.pow(loc.getLatitude() - mLastLocation.getLatitude(), 2)) / (loc.getTime() - this.mLastLocation.getTime()));
            //if there is speed from location
            mensaje8.setText(Double.toString(speed));
            if (loc.hasSpeed())
                //get location speed
                speed = loc.getSpeed();
            speed = loc.getSpeed();

            this.mLastLocation = loc;
            ////////////
            //DO WHAT YOU WANT WITH speed VARIABLE

            ////////////

           /* if (loc.hasSpeed()) {
                mensaje8.setText(Float.toString(loc.getSpeed()));

            } else {
                mensaje8.setText("loc.getspped no devuelve/tiene nada");
            }

            */


            /*conversion_float_int(loc.getSpeed()); */

            Location locationA = new Location("posicion bus");

            locationA.setLatitude(loc.getLatitude());
            locationA.setLongitude(loc.getLongitude());

            Location locationB = new Location("posicion usuario");

           /* locationB.setLatitude(latB);
            locationB.setLongitude(lngB);*/

            float distance = locationA.distanceTo(locationB);

            String Text = "Mi ubicacion actual es: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = " + loc.getLongitude();
            mensaje5.setText(Text);
            this.mainActivity.setLocation(loc);

            consulta_de_mysql="UPDATE datos SET longitud="+loc.getLongitude()+ ", latitud="+loc.getLatitude()+" WHERE id='1'";
           // new eject_cons().execute();
        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            mensaje1.setText("GPS Desactivado");
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado
            mensaje1.setText("GPS Activado");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // Este metodo se ejecuta cada vez que se detecta un cambio en el
            // status del proveedor de localizacion (GPS)
            // Los diferentes Status son:
            // OUT_OF_SERVICE -> Si el proveedor esta fuera de servicio
            // TEMPORARILY_UNAVAILABLE -> Temporalmente no disponible pero se
            // espera que este disponible en breve
            // AVAILABLE -> Disponible
        }



    }/* Fin de la clase localizacion */




}
