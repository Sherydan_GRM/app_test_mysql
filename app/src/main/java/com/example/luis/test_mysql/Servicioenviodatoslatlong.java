package com.example.luis.test_mysql;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import android.os.Vibrator;

public class Servicioenviodatoslatlong extends Service {
    String consulta_de_mysql;
    public Servicioenviodatoslatlong() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void onCreate(){
        super.onCreate();
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Servicioenviodatoslatlong.Localizacion Local = new Localizacion();
        //Local.setMainActivity(this);
        try{
            mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
                    (LocationListener) Local);
        }
        catch (SecurityException e){

        }
    }
    public void onStart(Intent intent, int startId){
        System.out.println("El servicio a Comenzado");
        this.stopSelf();
    }
    public void onDestroy(){
        super.onDestroy();
        System.out.println("El servicio a Terminado");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }








    //creo metodos para luego llamar en un async task dentro de de este mismo servicio
    class eject_cons extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params){
            ejecutar_mysql(consulta_de_mysql);
            return null;
        }
        @Override
        protected void onPostExecute(String result){
            /*limpiar variable que contiene/contenia la consulta para evitar posibles errores*/
            consulta_de_mysql="";
            Toast tostada = Toast.makeText(getBaseContext(),"consulta servicio ejecutada",Toast.LENGTH_SHORT);
            tostada.show();

            /*Vibrator mVibrator  = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            mVibrator.vibrate(1000);*/


        }

    }


    public void ejecutar_mysql(String recibo_cons) {
        try {
            String url = "jdbc:mysql://170.239.84.36/coordenadas_gps";
            String user = "gps_usuario";
            String password = "aiep";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = DriverManager.getConnection(url, user, password);
            Statement st = con.createStatement();

            st.executeUpdate(recibo_cons);
        }
        catch (Exception e){
          e.printStackTrace();
        }
    }
    public class Localizacion implements LocationListener {
        MainActivity mainActivity;
        private Location mLastLocation;
        public MainActivity getMainActivity() {
            return mainActivity;
        }
        public void setMainActivity(MainActivity mainActivity) {
            this.mainActivity = mainActivity;
        }
        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion
            loc.getLatitude();
            loc.getLongitude();
            loc.getSpeed();
            loc.hasSpeed();

            Location locationA = new Location("posicion bus");

            locationA.setLatitude(loc.getLatitude());
            locationA.setLongitude(loc.getLongitude());

            Location locationB = new Location("posicion usuario");

            float distance = locationA.distanceTo(locationB);

            String Text = "Mi ubicacion actual es: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = " + loc.getLongitude();

            //this.mainActivity.setLocation(loc);


            consulta_de_mysql="UPDATE datos SET longitud="+loc.getLongitude()+ ", latitud="+loc.getLatitude()+" WHERE id='3'";
            new eject_cons().execute();
        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado

        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // Este metodo se ejecuta cada vez que se detecta un cambio en el
            // status del proveedor de localizacion (GPS)
            // Los diferentes Status son:
            // OUT_OF_SERVICE -> Si el proveedor esta fuera de servicio
            // TEMPORARILY_UNAVAILABLE -> Temporalmente no disponible pero se
            // espera que este disponible en breve
            // AVAILABLE -> Disponible
        }



    }/* Fin de la clase localizacion */

}
